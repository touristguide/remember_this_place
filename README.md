Remember This Place
===================
**Introduction**<br/>
Did you ever feel frustrating to remember addresses like 12/455,xyz society,xyz city, bla bla bla.. or places like 'blablabla' coffe bar? Do you have a problem that you can't remember everyone's address? or Do you forget everytime places you visit? Do you want to track all the places you have visited in your journey? Then, this repository consists of Android application which is based on this idea. It will track your location and remember places you visit. So, now you can save places using your own name. Later, you can track those places on maps. Application is built using Android Studio and supports version higher than Android 4.0 ICS. It requires GPS access and network access. It makes use of location APIs (specially geocoding) for retrieving and showing location, SQLite for storing places and fragments for user interfaces.<br/>

**Screenshots**<br/>
Here are some screenshots of the application:<br/><br/>
![Screenshot](/../master/screenshots/screenshot11.png?raw=true "Home Screen")<br/>

![Screenshot](/../master/screenshots/screenshot13.png?raw=true "Remember place")<br/>
![Screenshot](/../master/screenshots/screenshot15.png?raw=true "Show places")<br/>

![Screenshot](/../master/screenshots/screenshot16.png?raw=true "Delete place")<br/>
![Screenshot](/../master/screenshots/screenshot17.png?raw=true "Track on Maps")<br/>

**Third party Libraries**
- ListViewAnimations:<br/> ListViewAnimations library is copyrighted work by Niek Haarman licensed under the Apache License, Version 2.0. See here for more details: https://github.com/nhaarman/ListViewAnimations<br/>
- ActionBar-PullToRefresh:<br/> ActionBar-PullToRefresh library is copyrighted work by Chris Banes licensed under the Apache License, Version 2.0. See here for more details: https://github.com/chrisbanes/ActionBar-PullToRefresh<br/>

**Apk**<br/>
Released apk is <a href="https://github.com/krupalshah/Remember_This_Place/blob/master/apk/app-release.apk">here.</a> Application requires Android 4.0 Ics or higher version.<br/>  

**License**<br/>
```
Copyright 2014 Krupal Shah 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```

package com.krupal.rememberthisplace;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


public class MapsFragment extends Fragment {
  public GoogleMap googleMap;
    CameraPosition cameraPosition;
Double latitude,longitude;
    String placename;
Marker marker;
    View rootView;

    MarkerOptions markeroptions;
    public MapsFragment()
    {


    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


       rootView = inflater.inflate(R.layout.fragment_maps, container, false);


        try {
            // Loading map
            initilizeMap();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return rootView;
    }

   public void initilizeMap() {
        if (googleMap == null) {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(
                    R.id.mymap)).getMap();
            // latitude and longitude
            Bundle bundle = this.getArguments();
            if(bundle != null){
                placename = bundle.getString("dbplacename");
                latitude = bundle.getDouble("dblatitude");
                longitude = bundle.getDouble("dblongitude");

                Log.d("my"," "+placename+"  "+ latitude+"  "+longitude);
            }


            googleMap.setMyLocationEnabled(true);


            googleMap.getUiSettings().setMyLocationButtonEnabled(true);
            googleMap.getUiSettings().setCompassEnabled(true);

            googleMap.getUiSettings().setAllGesturesEnabled(true);
            googleMap.getUiSettings().setZoomControlsEnabled(true);
            googleMap.getUiSettings().setRotateGesturesEnabled(true);

            //getting current location


            // create marker

       markeroptions= new MarkerOptions().position(new LatLng(latitude, longitude)).title(placename);
            markeroptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
            // adding marker



          marker = googleMap.addMarker(markeroptions);
            marker.showInfoWindow();
            cameraPosition = new CameraPosition.Builder().target(
                    new LatLng(latitude, longitude)).zoom(9).build();

            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(getActivity().getBaseContext(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    @Override
      public void onDestroyView() {
        super.onDestroyView();
        MapFragment f = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.mymap);
        if (f != null)
            getFragmentManager().beginTransaction().remove(f).commit();
    }
}
package com.krupal.rememberthisplace;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.nhaarman.listviewanimations.appearance.simple.ScaleInAnimationAdapter;
import com.nhaarman.listviewanimations.itemmanipulation.DynamicListView;
import com.nhaarman.listviewanimations.itemmanipulation.swipedismiss.OnDismissCallback;

import java.util.List;

public class PlaceholderFragment extends Fragment {
        public DynamicListView listview_places;
    String dbplacename;
    Double dblongitude;
    Double dblatitude;
    MapsFragment maps;
    ProgressDialog pd;

    public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_show_place, container, false);



            final MySqliteHelper db = new MySqliteHelper(getActivity().getBaseContext());

           List<Place> places = db.getAllPlaces();




            for (Place p : places){
                String log = "name:" +p.getname() + " ,details: " + p.getplacedetails();

                Log.d("my", log);
            }

            listview_places = (DynamicListView)rootView.findViewById(R.id.listView_places);

            final PlacesAdapter places_adapter = new PlacesAdapter(getActivity(),R.layout.listview_item_row,places);

            Log.d("my", "adapter constructor is working");

          ScaleInAnimationAdapter animationAdapter = new ScaleInAnimationAdapter(places_adapter);
            animationAdapter.setAbsListView(listview_places);

            listview_places.setAdapter(animationAdapter);

   //listview_places.enableDragAndDrop();
/*            listview_places.setDraggableManager(new TouchViewDraggableManager(R.id.list_row_draganddrop_touchview));
*/

           listview_places.enableSwipeToDismiss(
                    new OnDismissCallback() {
                        @Override
                        public void onDismiss(@NonNull final ViewGroup listView, @NonNull final int[] reverseSortedPositions) {
                            for (int position : reverseSortedPositions) {
                                Place place = places_adapter.getItem(position);

                                db.deletePlaces(place);

                                places_adapter.remove(place);
                                places_adapter.notifyDataSetChanged();
                            }
                        }
                    }
            );




            listview_places.setLongClickable(true);
            Log.d("my", "setAdapter() is working");
            listview_places.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position,long id) {

                    pd = new ProgressDialog(getActivity());

                    pd.show();
                    Place place = places_adapter.getItem(position);
                   Long db_id = place.getID();

                  Place selectedplace =  db.getPlace(db_id);


                    dblatitude = selectedplace.getlatitude();

                    dblongitude = selectedplace.getlongitude();


                    dbplacename = place.getname();

                   maps = new MapsFragment();
                    Bundle bundle = new Bundle();
                    bundle.putDouble("dblatitude",dblatitude);
                    bundle.putDouble("dblongitude",dblongitude);
                    bundle.putString("dbplacename",dbplacename);
                   maps.setArguments(bundle);

                    pd.dismiss();

                    FragmentManager fm = getFragmentManager();

                    FragmentTransaction ft = fm.beginTransaction();

                    ft.replace(R.id.container_showplaces,maps);

                    ft.addToBackStack(null);

                    ft.commit();

                }



        });


            listview_places.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                 /*       @Override
                        public boolean onItemLongClick(final AdapterView<?> parent, final View view,
                                                       final int position, final long id) {
                            listview_places.startDragging(position);
                            return true;
                        }
                    });
*/



                public boolean onItemLongClick(final AdapterView<?> parent, View view,
                                               final int pos, long id) {



                    new AlertDialog.Builder(getActivity())
                            .setTitle("Delete Place")
                            .setMessage("Are you sure you want to delete this place?")
                            .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    pd = new ProgressDialog(getActivity());
                                    pd.setMessage("deleting");
                                    pd.show();

                                    Place place = places_adapter.getItem(pos);

                                    db.deletePlaces(place);

                                    places_adapter.remove(place);
                                    places_adapter.notifyDataSetChanged();

                                    pd.dismiss();

                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            })
                            .setIcon(android.R.drawable.ic_menu_delete)
                            .show();

                    return true;
                }
            });


db.close();



            return rootView;
        }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(pd!=null){
            pd.dismiss();
        }
    }


    }


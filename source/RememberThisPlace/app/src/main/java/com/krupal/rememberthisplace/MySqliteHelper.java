package com.krupal.rememberthisplace;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class MySqliteHelper extends SQLiteOpenHelper{

	private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_NAME = "MyApplicationDatabase";
    
    private static final String KEY_ID = "id";
    private static final String KEY_PLACENAME = "placename";
    private static final String KEY_PLACEDETAILS = "placedetails";
    private static final String KEY_LATITUDE = "latitude";
    private static final String KEY_LONGITUDE = "longitude";

	private static final String TABLE_NAME = "places";
 
    
	public MySqliteHelper(Context context) {
		super(context, DATABASE_NAME,null, DATABASE_VERSION);

	
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		
	
		
		 //actual query = create table places (id primary key autoincrement, placename taxt, latitude real, longitude real);
		String query =  "CREATE TABLE " +TABLE_NAME + "( " + KEY_ID+
                " INTEGER PRIMARY KEY, " + KEY_PLACENAME+
                " TEXT, "+ KEY_PLACEDETAILS+
                " TEXT, "+ KEY_LATITUDE+
                " REAL, "+KEY_LONGITUDE+ " REAL)";
	 
	     
	          	db.execSQL(query);
	            Log.d("my", "Successfully created table: " + query);
	     
	    
		
	}

	
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS places");
		  
        this.onCreate(db);
	}

	
	public void addPlaces(Place place)
	{

        SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues convalues = new ContentValues();



		convalues.put(KEY_PLACENAME,place.getname());
		convalues.put(KEY_LATITUDE,place.getlatitude());
		convalues.put(KEY_LONGITUDE,place.getlongitude());
        convalues.put(KEY_PLACEDETAILS,place.getplacedetails());
		
		long newRowId =  db.insert(TABLE_NAME, null, convalues);
		  Log.d("my","db.insert(TABLE_NAME, null, convalues)");
		  Log.d("my", "Values inserted");

place.setID(newRowId);
		      db.close();



	}

    public Place getPlace(long id) {
        Place place;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("select * from " + TABLE_NAME + " where " + KEY_ID + "='" + id + "'" , null);
        place = new Place();




        if (cursor!=null && cursor.moveToFirst()) {


            place.setID(cursor.getLong(cursor.getColumnIndex(KEY_ID)));
            place.setname(cursor.getString(cursor.getColumnIndex(KEY_PLACENAME)));
            place.setlatitude(cursor.getDouble(cursor.getColumnIndex(KEY_LATITUDE)));
            place.setlongitude(cursor.getDouble(cursor.getColumnIndex(KEY_LONGITUDE)));
            place.setPlacedetails(cursor.getString(cursor.getColumnIndex(KEY_PLACEDETAILS)));
        }


        return place;
    }

    public List<Place> getAllPlaces() {
   List<Place> placeList = new ArrayList<Place>();



        SQLiteDatabase db = this.getWritableDatabase();

        String selectQuery = "SELECT  * FROM " + TABLE_NAME;

        Cursor cursor = db.rawQuery(selectQuery, null);
        Log.d("my","query fired");
        Log.d("my",cursor.toString());

        if (cursor.moveToFirst()) {

            Log.d("my","in movetofirst()");

            do {

                Log.d("my","in do");
                Place place = new Place();
                place.setID(cursor.getLong(cursor.getColumnIndex(KEY_ID)));
                place.setname(cursor.getString(cursor.getColumnIndex(KEY_PLACENAME)));
                place.setlatitude(cursor.getDouble(cursor.getColumnIndex(KEY_LATITUDE)));
                place.setlongitude(cursor.getDouble(cursor.getColumnIndex(KEY_LONGITUDE)));
                place.setPlacedetails(cursor.getString(cursor.getColumnIndex(KEY_PLACEDETAILS)));

                placeList.add(place);
            } while (cursor.moveToNext());
        }


        return placeList;
    }

    public void deletePlaces(Place place)
    {
        SQLiteDatabase db = this.getWritableDatabase();

       long id = place.getID();

        Log.d("my",Long.toString(id));
        db.delete(TABLE_NAME, KEY_ID + " = ?",
                new String[]{Long.toString(id)} );
        db.close();
    }

    public int updatePlaces(Place place) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_PLACENAME, place.getname());
        values.put(KEY_PLACEDETAILS, place.getplacedetails());


        return db.update(TABLE_NAME, values, KEY_ID + " = ?",
                new String[] { String.valueOf(place.getID()) });
    }


	
}

package com.krupal.rememberthisplace;

public class Place {
    long _id;
	private String _name;
    private double _latitude,_longitude;
    private String _placedetails;


    public Place(long id, String name, double latitude, double longitude,String placedetails)
		{
            this._id = id;
		  this._name = name;
		  this._latitude = latitude;
            this._longitude = longitude;
            this._placedetails = placedetails;


	}

    public Place(String name, double latitude, double longitude,String placedetails)
    {

        this._name = name;
        this._latitude = latitude;
        this._longitude = longitude;
        this._placedetails = placedetails;

    }

	public Place() {

	}
    public long getID(){
        return this._id;
    }


    public void setID(long id){
        this._id = id;
    }

    public void setname(String placename)
	{
        this._name = placename;


	}

	public String getname()
	{
		return this._name;


	}

	public void setlatitude(double latitude)
	{
        this._latitude=latitude;


	}

	public double getlatitude()
	{
		return this._latitude;


	}


	public void setlongitude(double longitude)
	{
        this._longitude = longitude;


	}

	public double getlongitude()
	{
		return this._longitude;


	}


    public void  setPlacedetails(String placedetails)
    {

        this._placedetails = placedetails;
    }


    public String getplacedetails()
    {
        return this._placedetails;
    }


}

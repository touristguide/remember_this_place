package com.krupal.rememberthisplace;


import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class PlacesAdapter extends ArrayAdapter<Place>{

    Context mcontext;
    int mlayoutResourceId;
  List<Place> mdata = null;
    View row;


    public PlacesAdapter(Context context, int layoutResourceId, List<Place> data) {
        super(context, layoutResourceId, data);
        mlayoutResourceId = layoutResourceId;
        mcontext = context;
        mdata = data;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        row = convertView;
        PlaceHolder holder = null;

        if(row == null)
        {
            LayoutInflater inflater = ( (Activity) mcontext).getLayoutInflater();
            row = inflater.inflate(mlayoutResourceId, parent, false);

            holder = new PlaceHolder(row);


            row.setTag(holder);
            Log.d("my","new row is inflated for listview current position");

        }
        else
        {
            holder = (PlaceHolder)row.getTag();
            Log.d("my","row is taken from the holder");
        }

        Place p = mdata.get(position);
        holder.txt_place_title.setText(p.getname());
        holder.txt_place_details.setText(p.getplacedetails());

        return row;
    }

    class PlaceHolder
    {
        TextView txt_place_title;
        TextView txt_place_details;


        public PlaceHolder(View v){

            txt_place_title = (TextView)row.findViewById(R.id.txt_Place_Title);
            txt_place_details = (TextView)row.findViewById(R.id.txt_Place_Details);

        }
    }
}


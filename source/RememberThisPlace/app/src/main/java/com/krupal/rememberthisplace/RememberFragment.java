package com.krupal.rememberthisplace;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 *  Created by Jainam on 01/03/15.
 */
public class RememberFragment extends Fragment {
    double mylatitude, mylongitude;
    String myplacename, myplacedetails;
   EditText mpostplacename, mpostplacedetails;
    Button addplaces;
    ProgressDialog pd;
    GPSTracker gps;

  //  @InjectView(R.id.edittext_placename) EditText mpostplacename;
  //  @InjectView(R.id.edittext_placedetails)EditText mpostplacedetails;

  //  @InjectView(R.id.addplace_btn)Button addplaces;


    public RememberFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_remember, container, false);
      mpostplacename = (EditText)root.findViewById(R.id.edittext_placename);


       mpostplacedetails = (EditText)root.findViewById(R.id.edittext_placedetails);

    addplaces = (Button)root.findViewById(R.id.addplace_btn);
        new BackgroundOperation().execute("");
        return root;
    }
    @Override
    public void onActivityCreated(Bundle b)
    {
        super.onActivityCreated(b);

        addplaces.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pd = new ProgressDialog(getActivity());
                pd.setMessage("saving");
                pd.show();

                myplacename = mpostplacename.getText().toString();
                myplacedetails = mpostplacedetails.getText().toString();

                if(myplacename.matches(""))
                {

                    String latis = Double.toString(mylatitude);
                    String longis = Double.toString(mylongitude);
                    myplacename = "Latitude :"+latis+"   "+"Longitude :"+longis;


                }
                if(myplacedetails.matches(""))
                {
                    myplacedetails = "No details given about this place";
                }
                MySqliteHelper db = new MySqliteHelper(getActivity().getBaseContext());


                Log.d("my", "place name adding :" + myplacename);
                Log.d("my","place details adding :"+myplacedetails);




                Place place =  new Place(myplacename, mylatitude, mylongitude, myplacedetails);
                db.addPlaces(place);
                pd.dismiss();

                Toast toast = Toast.makeText(getActivity(), "Ok! App will remember this place", Toast.LENGTH_LONG);
                toast.show();
                Intent i = new Intent(getActivity(), MainActivity.class);
                startActivity(i);

            }
        });

    }
    private class BackgroundOperation extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            gps = new GPSTracker(getActivity());


            if (gps.canGetLocation()) {

                mylatitude = gps.getLatitude();
                mylongitude = gps.getLongitude();


               // mylatitude = 23.89;
              //  mylongitude=75.67;

            }
            return  " ";
           }
    }
}
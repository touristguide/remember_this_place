package com.krupal.rememberthisplace;

import android.app.Fragment;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;


public class MainPlaceholderFragment extends Fragment {


   // @InjectView(R.id.current_location) TextView txt_location;


    GPSTracker gps;
    double mylatitude, mylongitude;
  TextView txt_location;
    String locationstring = " ";
    private PullToRefreshLayout mPullToRefreshLayout;
    public MainPlaceholderFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_main, container,
                false);
    txt_location = (TextView) view.findViewById(R.id.current_location);




        new LongOperation().execute("");

        mPullToRefreshLayout = (PullToRefreshLayout)getActivity().findViewById(R.id.ptr_layout);



        return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {



        super.onViewCreated(view,savedInstanceState);


        ActionBarPullToRefresh.from(getActivity())

                .allChildrenArePullable()

                .listener(new OnRefreshListener() {
                    @Override
                    public void onRefreshStarted(View view) {

                        new LongOperation().execute();


                    }
                }).setup(mPullToRefreshLayout);


    }


    public class LongOperation extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {
                gps = new GPSTracker(getActivity());


                if (gps.canGetLocation()) {

                    mylatitude = gps.getLatitude();
                    mylongitude = gps.getLongitude();



              // mylatitude = 23.89;
              // mylongitude=75.67;


                    String latis = Double.toString(mylatitude);
                    String longis = Double.toString(mylongitude);
                    Log.d("my", latis + "location to string is done   " + longis);

                    Geocoder geocoder = new Geocoder(getActivity(), Locale.ENGLISH);

                    try {

                        List<Address> addresses = geocoder.getFromLocation(mylatitude, mylongitude, 1);
                        if (addresses != null && !addresses.isEmpty()) {


                            String myaddress = addresses.get(0).getAddressLine(0);
                            String mycity = addresses.get(0).getAddressLine(1);
                            String mycode = addresses.get(0).getAddressLine(2);
                            String mycountry = addresses.get(0).getAddressLine(3);

                            String array_check[] = {myaddress, mycity, mycode, mycountry};

                            for (int i = 0; i < 4; i++) {
                                if (array_check[i] == null) {
                                    array_check[i] = " ";
                                }
                                locationstring = locationstring + array_check[i] + " ";
                            }

                        } else {
                            locationstring = "No Address found! " + " Latitude: " + latis + " Longitude: " + longis;
                        }
                    }
                    catch (IOException e) {

                        e.printStackTrace();
                       locationstring ="Couldn't get Address!"+" Latitude: "+latis +" Longitude: "+longis;
                    }
                }
                return locationstring;
            }

         @Override
         protected void onPostExecute(String result) {
             txt_location.setText(locationstring);
             mPullToRefreshLayout.setRefreshComplete();


         }

         @Override
         protected void onPreExecute() {super.onPreExecute();}

         @Override
         protected void onProgressUpdate(Void... values) {super.onProgressUpdate();}
     }



}